cf-ecs-devops
=============
Role to delete the devsps Cloudformation stacks

Requirements
------------
N/A

Role Variables
--------------
N/A

Dependencies
------------
N/A

Example Playbook
----------------
Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: localhost
      roles:
         - cf-ecs-devops

License
-------
Zift Solutions, Inc.

Author Information
------------------
Chris Fouts
